#include <windows.h>//���������� ��� �������������� � win api
#pragma comment(lib, "Msimg32.lib")
#include <stdlib.h>
#include <string.h>
#include <tchar.h>

static TCHAR szWindowClass[] = _T("TriangleApp");
static TCHAR szTitle[] = _T("Triangle");

HINSTANCE hInst;
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//�������� ����� �����
int WINAPI WinMain(
	HINSTANCE hInstance, //������ ������������ ������
	HINSTANCE,
	LPSTR     lpCmdLine, // ��������� �� ������
	int       nCmdShow //������� ������ ����
) {
	MSG msg{}; //���������� ���������
	HWND hwnd{};//���������� ����
	WNDCLASSEX wc{ sizeof(WNDCLASSEX) }; //�������������� ���� (���������� ������������� �������)
	//���������� ���������
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = reinterpret_cast<HBRUSH>(GetStockObject(WHITE_BRUSH));
	wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wc.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
	wc.hIconSm = LoadIcon(nullptr, IDI_APPLICATION);
	wc.hInstance = hInstance;
	wc.lpfnWndProc = WndProc; //��������� ���������
	wc.lpszClassName = L"MyAppClass";
	wc.lpszMenuName = nullptr;
	wc.style = CS_VREDRAW | CS_HREDRAW;

	//����������� ���� � �������
	if (!RegisterClassEx(&wc))
		return EXIT_FAILURE;

	//������� ����
	if (hwnd = CreateWindow(wc.lpszClassName, L"��������_1 - �����������", WS_OVERLAPPEDWINDOW, 0, 0, 800, 500, nullptr, nullptr, wc.hInstance, nullptr), hwnd == INVALID_HANDLE_VALUE)
		return EXIT_FAILURE;

	//���������� ����
	ShowWindow(hwnd, nCmdShow);
	UpdateWindow(hwnd);

	//���� ��������� ���������
	while (GetMessage(&msg, nullptr, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return static_cast<int>(msg.wParam);

};

HBITMAP hBitmap = NULL;

//��������� ��������
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	UINT color = RGB(32, 192, 64);

	switch (message)
	{
	case WM_CREATE:
		
		hBitmap = (HBITMAP)LoadImage(NULL, L"�����������.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);//�������� � ������
		break;
	case WM_PAINT:

		PAINTSTRUCT     ps;
		HDC             hdc, hdcMem, hdcMem1;
		BITMAP          bitmap;
		HGDIOBJ         oldBitmap, oldBitmap1;
		
		RECT rect;
		GetClientRect(hWnd, &rect);

		hdc = BeginPaint(hWnd, &ps);
		hdcMem1 = GetDC(hWnd);

		hdcMem = CreateCompatibleDC(hdc);
		
		oldBitmap = SelectObject(hdcMem, hBitmap);
		//��������� ���������� � ����������� �����������
		GetObject(hBitmap, sizeof(BITMAP), (LPSTR) & bitmap); 
		//����� ����������� � ������� �������� �����
		TransparentBlt(hdcMem1, 0, 0, bitmap.bmWidth, bitmap.bmHeight, hdcMem, 0, 0, bitmap.bmWidth, bitmap.bmHeight, color);
		
		SelectObject(hdcMem, oldBitmap);
		
		DeleteDC(hdcMem);

		DeleteDC(hdc);

		EndPaint(hWnd, &ps);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}


